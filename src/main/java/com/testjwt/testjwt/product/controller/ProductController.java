package com.testjwt.testjwt.product.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testjwt.testjwt.product.entity.Product;
import com.testjwt.testjwt.product.repo.ProductRepo;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    ProductRepo productRepo;

    
    @GetMapping
    public List<Product> list(){
        return productRepo.findAll();
    }

}
