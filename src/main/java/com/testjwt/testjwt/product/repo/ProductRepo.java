package com.testjwt.testjwt.product.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.testjwt.testjwt.product.entity.Product;

public interface ProductRepo  extends JpaRepository<Product, Integer>{
    
}
